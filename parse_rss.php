<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
require($_SERVER["DOCUMENT_ROOT"]."/novosti-s-khabra/func_filter.php");
$APPLICATION->SetPageProperty("title", "Парсинг с хабра");
$APPLICATION->SetTitle("Парсинг с хабра");
?>

<?php
CModule::IncludeModule("iblock");
$arRes = CIBlockRSS::GetNewsEx("habr.com", 80, "/ru/rss/all/all", '');
$arRes = CIBlockRSS::FormatArray($arRes);
//echo '<pre>'.print_r($arRes, 1).'</pre>';

$params = Array(
    "max_len" => "40", // обрезает символьный код до n символов
    "change_case" => "L", // буквы преобразуются к нижнему регистру
    "replace_space" => "_", // меняем пробелы на нижнее подчеркивание
    "replace_other" => "_", // меняем левые символы на нижнее подчеркивание
    "delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
    "use_google" => "false", // отключаем использование google
);
$arFields = array(
    "ACTIVE" => "Y",
    "IBLOCK_ID" => 1,
    "ACTIVE_FROM" => date('d.m.Y H:i:s'),
    "NAME" => "Новость",
    "CODE" => "news",
    "PREVIEW_TEXT" => "Превью",
    "DETAIL_TEXT" => "Описание элемента",
    "PROPERTY_VALUES" => array(
        "LINK" =>"Ссылка на статью",
    )
);
$oElement = new CIBlockElement();

foreach ($arRes['item'] as $post):

    $arFields['NAME'] = $post['title'];
    $arFields['CODE'] = CUtil::translit($post['title'], "ru" , $params);
    $date = date_create($post['pubDate']);
    $arFields['ACTIVE_FROM'] = date_format($date, 'd.m.Y H:i:s');
    $arFields['PREVIEW_TEXT'] = $post['description'];
    $arFields['PROPERTY_VALUES']['LINK'] = $post['link'];

    if (!isExistPost($post['link'])) {
        $idElement = $oElement->Add($arFields, false, false, true);
    }
    ?>

    <h2 class=""><?=$arFields['NAME']; ?></h2>
    <p><?=$arFields['ACTIVE_FROM'];?></p>
    <p ><?=$arFields['PREVIEW_TEXT'];?></p>
    <a href="<?=$arFields['PROPERTY_VALUES']['LINK']; ?>"><?=$arFields['PROPERTY_VALUES']['LINK']; ?></a><br>


<?php endforeach; ?>

<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

