<?php
function isExistPost($link)
{
    $return = false;
    $filter = [
        'IBLOCK_ID' => 1,
        'PROPERTY_LINK' => $link,
    ];
    $select = ['ID'];

    $postFiltr = CIBlockElement::GetList([],$filter, false, $select);

    if ($postFiltr->GetNext()) {
        $return = true;
    }

    return $return;
}

